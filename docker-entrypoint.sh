#!/bin/bash

MYSQL_DB_HOST="${MYSQL_DB_HOST:-mysql}"
MYSQL_DB_USER="${MYSQL_DB_USER:-mysql}"
MYSQL_DB_PASS="${MYSQL_DB_PASS:-mysql}"

mkdir -p /var/lib/mysql

if [ ! -f "/var/lib/mysql/.restored" ]; then
    for f in /docker-entrypoint-initdb.d/*; do
        case "$f" in
            *.sh)     echo "$0: running $f"; . "$f" ;;
            *.sql)    echo "$0: running $f"; /usr/bin/mysql -u${MYSQL_DB_USER} -h${MYSQL_DB_HOST} -p${MYSQL_DB_PASS} < "$f"; echo ;;
            *.sql.gz) echo "$0: running $f"; gunzip -c "$f" | /usr/bin/mysql -u${MYSQL_DB_USER} -h${MYSQL_DB_HOST} -p${MYSQL_DB_PASS}; echo ;;
            *)        echo "$0: ignoring $f" ;;
        esac
        echo
    done
    touch /var/lib/mysql/.restored
fi

exec "$@"
